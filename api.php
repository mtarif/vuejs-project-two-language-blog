<?php

	$en = [
		"date" => "2020-04-25",
		"explanation" => "These bright ridges of interstellar gas and dust are bathed in energetic starlight. With its sea of young stars, the massive star-forming region NGC 2014 has been dubbed the Cosmic Reef. Drifting just off shore, the smaller NGC 2020, is an expansive blue-hued structure erupting from a single central Wolf-Rayet star, 200,000 times brighter than the Sun. The cosmic frame spans some 600 light-years within the Large Magellanic Cloud 160,000 light-years away, a satellite galaxy of our Milky Way. A magnificent Hubble Space Telescope portrait, the image was released this week as part of a celebration to mark Hubble's 30th year exploring the Universe from Earth orbit.",
		"hdurl" => "https://apod.nasa.gov/apod/image/2004/STSCI-H-p2016a-m-2000x1374.jpg",
		"media_type" => "image",
		"service_version" => "v1",
		"title" => "Hubble's Cosmic Reef",
		"url" => "https://apod.nasa.gov/apod/image/2004/STSCI-H-p2016a-z-1000x687.jpg",
	];

	$fa = [
		"date" => "۶ اردیبهشت ۱۳۹۹",
		"explanation" => "این پشته های درخشان از گاز و غبار بین ستاره ای در نور ستاره پرانرژی غرق می شوند. با وجود دریای ستاره های جوان ، منطقه عظیم ستاره ساز NGC 2014 لقب کیهان کیهانی گرفته است. NGC 2020 کوچکتر از NGC 2020 ، ساختاری گسترده ای است که از رنگ آبی متمایز است که از یک ستاره مرکزی گرگ-ریایت ، 200000 بار درخشان تر از خورشید در حال فوران است. قاب کیهانی حدود 600 سال نوری در ابر بزرگ ماژلانیک 160،000 سال نوری دور ، کهکشان ماهواره ای از کهکشان راه شیری ما است. این تصویربرداری با شکوه از تلسکوپ فضایی هابل ، این هفته به عنوان بخشی از یک جشن برای نشانه سی سالگی هابل در کاوش جهان از مدار زمین منتشر شد.",
		"hdurl" => "https://apod.nasa.gov/apod/image/2004/STSCI-H-p2016a-m-2000x1374.jpg",
		"media_type" => "image",
		"service_version" => "v1",
		"title" => "صخره کیهانی هابل",
		"url" => "https://apod.nasa.gov/apod/image/2004/STSCI-H-p2016a-z-1000x687.jpg",
	];

	header('Content-type: application/json');
	header("Access-Control-Allow-Origin: *");

	if ($_GET['lang'] == 'fa') {
		echo json_encode($fa);
	} else {
		echo json_encode($en);
	}

?>